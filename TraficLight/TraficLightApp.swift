//
//  TraficLightApp.swift
//  TraficLight
//
//  Created by Дмитрий on 18.08.2023.
//

import SwiftUI

@main
struct TraficLightApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
