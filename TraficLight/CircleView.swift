//
//  CircleView.swift
//  TraficLight
//
//  Created by Дмитрий on 18.08.2023.
//

import SwiftUI

struct CircleView: View {
    let color: Color
    let opacity: Double
    var body: some View {
        Circle()
            .foregroundColor(color)
            .frame(width: 150, height: 150)
            .opacity(opacity)
        
    }
}

struct CircleView_Previews: PreviewProvider {
    static var previews: some View {
        CircleView(color: .red, opacity: 0.7)
    }
}
