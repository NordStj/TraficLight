//
//  ContentView.swift
//  TraficLight
//
//  Created by Дмитрий on 18.08.2023.
//

import SwiftUI

enum CurrentLight {
    case red, yellow, green
}

struct ContentView: View {

    @State var lightCircle = [0.3, 0.3, 0.3]
    @State var currentLight = CurrentLight.red
    @State var textButton = "START"
    
    var body: some View {
        VStack {
            CircleView(color: .red, opacity: lightCircle[0])
            CircleView(color: .yellow, opacity: lightCircle[1] )
            CircleView(color: .green, opacity: lightCircle[2])
            Spacer()
            button
        }
        .padding()
    }
    private var button: some View{
        Button(action: switchLight) {
            Text(textButton)
                .foregroundColor(.white)
                .font(.title)
    }
        .background(
            Capsule()
                .fill(Color(.systemBlue))
                .frame(width: 140, height: 40)
            )
    }
    
    private func switchLight() {
        if textButton == "START" {
            textButton = "NEXT"
        }
        
        switch currentLight {
        case .red:
            lightCircle = [1.0, 0.3, 0.3]
            currentLight = .yellow
        case .yellow:
            lightCircle = [0.3, 1.0, 0.3]
            currentLight = .green
        case .green:
            lightCircle = [0.3, 0.3, 1.0]
            currentLight = .red
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
